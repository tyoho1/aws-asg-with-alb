# aws-asg-with-alb

---
A terraform module to provide an Auto Scaling Group behind an Application Load Balancer in AWS.

There are no required input variables. You can use this module simply by defining a module block in your TF scripts and adding the appropriate source attribute.
When running this module with no defined input variables it creates an ASG and ALB that launches 1 instance running a basic Apache web server.

## Optional Module Input Variables

---
|Name|Type|Default|Description|
|:---:|:---:|:---:|---|
|name_prefix|string|`"sample-asg-w-alb"`|Name for prefix in naming convention of all resources.|
|vpc_id|string|Default VPC for the current region|ID of the VPC to launch AWS services into.|
|subnets|list(string)|All subnets associated with vpc_id|List of Subnet Ids to launch the instances created by the Auto Scaling Group into.|
|mandatory_tags|mapping of tags|`{}`|Mapping of tags to add to all taggable resources|
|instance_ami|string|Latest Amazon Linux 2 AMI|AMI to build the instance off of.|
|instance_type|string|`"t2.micro"`|Instance type for the instance created by the Auto Scaling Group.|
|instance_profile|string| - |Name of the instance profile for the instance created by the Auto Scaling Group.|
|instance_user_data|user_data|Basic Apache Web Server|User Data to run on instance launch.|
|application_port|number|`80`|The main port that your application is listening on in your instance.|
|asg_max_size|number|`1`|Maximum number of instances the Auto Scaling Group should have running.|
|asg_min_size|number|`1`|Minimum number of instances the Auto Scaling Group should have running.|
|asg_desired_capacity|number|`1`|Desired number of instances the Auto Scaling Group should have running.|
|asg_health_check_grace_period|number|`300`|The amount of time (in seconds) that the Auto Scaling Group should wait before checking health of the instance after it comes into service.|
|lb_internal|bool|`false`|Boolean of whether the Load Balancer should be internal.|
|deregistration_delay|number|`300`|The amout of time (in seconds) to drain an instance before setting it to unused.|
|health_check_path|string|`"/"`|Path to health check on instance.|
|health_check_port|string|application_port|Port for health check on instance.|
|health_check_healthy_threshold|number|`2`|Consecutive number of successfull health checks before determining an instance healthy.|
|health_check_unhealthy_threshold|number|`2`|Consecutive number of failed health checks before determining an instance unhealthy.|
|health_check_interval|number|`30`|Amount of time (in seconds) between health checks of an instance. |
|health_check_timeout|number|`5`|Amount of time (in seconds) that no response from the instance means a failed health check.|
|health_check_healthy_status_code|comma seperated string|`"200"`|HTTP status code that corresponds to a healthy response from an instance.|
|enable_lb_https|bool|`false`|Enable the Load Balancer to use HTTPS and forward all HTTP traffic to HTTPS.|
|lb_certificate_arn|string| - |ARN of the AWS ACM Certificate to provide an HTTPS listener on the Load Balancer. Required if enable_lb_https is true."|

## Usage

---
### Learning

#### Create a Sample Apache Web Server

```text
module "asg_w_alb" {
  source = "bitbucket.org/tyoho1/aws-asg-with-alb"
}
```

### Basic

#### Create a Custom Basic Application

```text
module "asg_w_alb" {
  source = "bitbucket.org/tyoho1/aws-asg-with-alb"
  name_prefix = "myWebServer"
  vpc_id = "vpc-123"
  instance_type = "c4.large"
  instance_profile = aws_iam_instance_profile.web_server.name

  mandatory_tags = {
      Costcenter = "CC12345"
      Environment = "DEV"
  }

  instance_user_data = <<EOF
#!/bin/bash
yum update -y
yum install httpd -y
systemctl start httpd
systemctl stop firewalld
cd /var/www/html
INSTANCE_ID=$(curl http://169.254.169.254/1.0/meta-data/instance-id)
echo "<h1>Test Apache Server from Instance: $${INSTANCE_ID}</h1>" > index.html
    EOF
}
```

### Advanced

#### Create an internal load balancer listening on HTTPS that runs an application from an Ansible Playbook

```text
module "asg_w_alb" {
  source = "bitbucket.org/tyoho1/aws-asg-with-alb"
  name_prefix = "myWebServer"
  vpc_id = "vpc-123"
  subnets = ["subnet-123", "subnet-456"]
  instance_ami = "ami-123"
  instance_type = "c4.large"
  instance_profile = aws_iam_instance_profile.web_server.name
  application_port = 8080
  asg_max_size = 6
  asg_min_size = 2
  asg_desired_capacity = 3
  asg_health_check_grace_period = 240
  lb_internal = true
  deregistration_delay = 120
  health_check_path = "/health"
  health_check_port = 8080
  health_check_healthy_threshold = 3
  health_check_unhealthy_threshold = 5
  health_check_interval = 15
  health_check_timeout = 8
  health_check_healthy_status_code = "200,301"
  enable_lb_https = true
  lb_certificate_arn = "arn:aws:acm:us-east-1:987654321:certificate/12345"

  mandatory_tags = {
    Costcenter = "CC12345"
    Environment = "DEV"
  }

  instance_user_data = <<EOF
#!/bin/bash
set -e
set -x
function main {
    declare -r url=$1 playbook=$2
    yum update -y
    yum install -y git
    yum install -y python37
    pip3 install ansible
    /usr/local/bin/ansible-pull --accept-host-key --verbose \
      --url "$url" --directory /var/local/src/instance-bootstrap "$playbook"
}
main \
  'https://github.com/sjparkinson/terraform-ansible-example.git' \
  'ansible/local.yml'
    EOF
}
```

## Outputs

---

- `load_balancer_dns_name` - DNS name of the Load Balancer
- `load_balancer_arn` - ARN of the Load Balancer
- `load_balancer_arn_suffix` - ARN suffix of the Load Balancer for use with CLoudWatch Metrics
- `load_balancer_zone_id` - The canonical hosted zone ID of the Load Balancer for use in Route53 Alias record
- `aws_autoscaling_group_arn` - ARN of the Auto Scaling Group
- `aws_autoscaling_group_name` - Name of the Auto Scaling Group
- `aws_autoscaling_group_target_group_arns` - List of ARNS of the Target Groups that apply to the Auto Scaling Group

## Authors

---
<tyoho@hgsdigital.com>
