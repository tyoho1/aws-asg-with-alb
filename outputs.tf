output "load_balancer_id" {
  value = aws_lb.main.id
}

output "load_balancer_arn" {
  value = aws_lb.main.arn
}

output "load_balancer_arn_suffix" {
  value = aws_lb.main.arn_suffix
}

output "load_balancer_dns_name" {
  value = aws_lb.main.dns_name
}

output "aws_autoscaling_group_id" {
  value = aws_autoscaling_group.main.arn
}

output "aws_autoscaling_group_name" {
  value = aws_autoscaling_group.main.name
}

output "aws_autoscaling_group_target_group_arns" {
  value = aws_autoscaling_group.main.target_group_arns
}
