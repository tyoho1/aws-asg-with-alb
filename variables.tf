variable "name_prefix" {
  type        = string
  description = "Name for prefix in naming convention of all resources. Default: 'sample-asg-w-alb'"
  default     = "sample-asg-w-alb"
}

variable "vpc_id" {
  type        = string
  description = "ID of the VPC to launch AWS services into. Defaults to the regions default VPC."
  default     = "default"
}

variable "subnets" {
  type        = list(string)
  description = "List of Subnet Ids to launch the instances created by the Auto Scaling Group into. Defaults to all subnets associated in the vpc_id variable."
  default     = []
}

variable "mandatory_tags" {
  description = "Mapping of tags to add to all taggable resources. Default: {}"
  default     = {}
}

variable "instance_ami" {
  type        = string
  description = "AMI to build the instance off of. Default to the latest Amazon Linux 2 AMI."
  default     = "default"
}

variable "instance_type" {
  type        = string
  description = "Instance type for the instance created by the Auto Scaling Group. Default: 't2.micro'"
  default     = "t2.micro"
}

variable "instance_profile" {
  type        = string
  description = "Name of the instance profile for the instance created by the Auto Scaling Group. Default to null"
  default     = "default"
}

variable "instance_user_data" {
  description = "User Data to run on instance launch. Default to running a basic Apache web server"
  default     = "default"
}

variable "application_port" {
  type        = number
  description = "The main port that your application is listening on in your instance. Default: 80"
  default     = 80
}

variable "asg_max_size" {
  type        = number
  description = "Maximum number of instances the Auto Scaling Group should have running. Default: 1"
  default     = 1
}

variable "asg_min_size" {
  type        = number
  description = "Minimum number of instances the Auto Scaling Group should have running. Default: 1"
  default     = 1
}

variable "asg_desired_capacity" {
  type        = number
  description = "Desired number of instances the Auto Scaling Group should have running. Default: 1"
  default     = 1
}

variable "asg_health_check_grace_period" {
  type        = number
  description = "The amount of time (in seconds) that the Auto Scaling Group should wait before checking health of the instance after it comes into service. Default: 300"
  default     = 300
}

variable "lb_internal" {
  type        = bool
  description = "Boolean of whether the Load Balancer should be internal. Default: false"
  default     = false
}

variable "deregistration_delay" {
  type        = number
  description = "The amout of time (in seconds) to drain an instance before setting it to unused. Default: 300"
  default     = 300
}

variable "health_check_path" {
  type        = string
  description = "Path to health check on instance. Default: '/'"
  default     = "/"
}

variable "health_check_port" {
  type        = string
  description = "Port for health check on instance. Defaults to application_port"
  default     = "traffic-port"
}


variable "health_check_healthy_threshold" {
  type        = number
  description = "Consecutive number of successfull health checks before determining an instance healthy. Default: 2"
  default     = 2
}

variable "health_check_unhealthy_threshold" {
  type        = number
  description = "Consecutive number of failed health checks before determining an instance unhealthy. Default: 2"
  default     = 2
}

variable "health_check_interval" {
  type        = number
  description = "Amount of time (in seconds) between health checks of an instance. Default: 30"
  default     = 30
}

variable "health_check_timeout" {
  type        = number
  description = "Amount of time (in seconds) that no response from the instance means a failed health check. Default: 5"
  default     = 5
}

variable "health_check_healthy_status_code" {
  type        = string
  description = "HTTP status code that corresponds to a healthy response from an instance. Default: '200'"
  default     = "200"
}

variable "enable_lb_https" {
  type        = bool
  description = "Enable the Load Balancer to use HTTPS and forward all HTTP traffic to HTTPS. Default: false"
  default     = false
}

variable "lb_certificate_arn" {
  type        = string
  description = "ARN of the AWS ACM Certificate to provide an HTTPS listener on the Load Balancer. Required if enable_lb_https is true. Default: null"
  default     = ""
}
