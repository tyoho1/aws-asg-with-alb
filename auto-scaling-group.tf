data "aws_ami" "latest_amz_linux_2" {
  most_recent = true

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["amazon"]
}

locals {
  user_data = <<EOF
#!/bin/bash
yum update -y
yum install httpd -y
systemctl start httpd
systemctl stop firewalld
cd /var/www/html
INSTANCE_ID=$(curl http://169.254.169.254/1.0/meta-data/instance-id)
echo "<h1>Test Apache Server from Instance: $${INSTANCE_ID}</h1>" > index.html
    EOF
}


resource "aws_launch_configuration" "main" {
  name_prefix                 = var.name_prefix
  image_id                    = var.instance_ami == "default" ? data.aws_ami.latest_amz_linux_2.id : var.instance_ami
  instance_type               = var.instance_type
  security_groups             = [aws_security_group.instance_sg.id]
  iam_instance_profile        = var.instance_profile == "default" ? null : var.instance_profile
  associate_public_ip_address = false
  user_data                   = var.instance_user_data == "default" ? local.user_data : var.instance_user_data

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "instance_sg" {
  name        = "${var.name_prefix}InstanceSG"
  description = "Only allows traffic from the Load Balancer"
  vpc_id      = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id

  ingress {
    from_port       = var.application_port
    to_port         = var.application_port
    protocol        = "tcp"
    security_groups = [var.enable_lb_https ? aws_security_group.lb_sg_with_https[0].id : aws_security_group.lb_sg_without_https[0].id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.mandatory_tags
}

resource "aws_autoscaling_group" "main" {
  name                      = "${var.name_prefix}ASG"
  max_size                  = var.asg_max_size
  min_size                  = var.asg_min_size
  health_check_grace_period = var.asg_health_check_grace_period
  health_check_type         = "ELB"
  desired_capacity          = var.asg_desired_capacity
  force_delete              = false
  launch_configuration      = aws_launch_configuration.main.name
  vpc_zone_identifier       = length(var.subnets) == 0 ? data.aws_subnet_ids.default.ids : var.subnets
  target_group_arns         = [aws_lb_target_group.main.arn]

  lifecycle {
    create_before_destroy = true
  }

  tags = concat(data.null_data_source.asg_merged_tag_list.*.outputs, list(map("key", "Name", "value", "${var.name_prefix}Instance", "propagate_at_launch", true)))
}

locals {
  keys   = keys(var.mandatory_tags)
  values = values(var.mandatory_tags)
}

data "null_data_source" "asg_merged_tag_list" {
  count = length(local.keys)

  inputs = {
    key                 = local.keys[count.index]
    value               = local.values[count.index]
    propagate_at_launch = true
  }
}
