resource "aws_lb" "main" {
  name               = "${var.name_prefix}LoadBalancer"
  internal           = var.lb_internal
  load_balancer_type = "application"
  security_groups    = [var.enable_lb_https ? aws_security_group.lb_sg_with_https[0].id : aws_security_group.lb_sg_without_https[0].id]
  subnets            = length(var.subnets) == 0 ? data.aws_subnet_ids.default.ids : var.subnets

  tags = var.mandatory_tags
}

resource "aws_lb_listener" "without_https" {
  count = var.enable_lb_https ? 0 : 1

  load_balancer_arn = aws_lb.main.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }
}

resource "aws_lb_listener" "with_https" {
  count = var.enable_lb_https ? 1 : 0

  load_balancer_arn = aws_lb.main.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"
    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}


resource "aws_lb_listener" "main" {
  count = var.enable_lb_https ? 1 : 0

  load_balancer_arn = aws_lb.main.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = var.lb_certificate_arn
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }
}

resource "aws_lb_target_group" "main" {
  name                 = "${var.name_prefix}TargetGroup"
  port                 = var.application_port
  protocol             = "HTTP"
  vpc_id               = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id
  deregistration_delay = var.deregistration_delay

  health_check {
    port                = var.health_check_port
    path                = var.health_check_path
    healthy_threshold   = var.health_check_healthy_threshold
    unhealthy_threshold = var.health_check_unhealthy_threshold
    interval            = var.health_check_interval
    timeout             = var.health_check_timeout
    matcher             = var.health_check_healthy_status_code
  }

  tags = var.mandatory_tags
}

resource "aws_security_group" "lb_sg_without_https" {
  count = var.enable_lb_https ? 0 : 1

  name        = "${var.name_prefix}LoadBalancerSG"
  description = "Allow all internet traffic"
  vpc_id      = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.mandatory_tags
}

resource "aws_security_group" "lb_sg_with_https" {
  count = var.enable_lb_https ? 1 : 0

  name        = "${var.name_prefix}LoadBalancerSG"
  description = "Allow all internet traffic"
  vpc_id      = var.vpc_id == "default" ? aws_default_vpc.default.id : var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = var.mandatory_tags
}
